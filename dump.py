#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import sys
# Bad namming: change namming
from AdafruitThermal import Adafruit_Thermal as AdafruitThermal
import subprocess, time, Image, socket
import os
import json

#
CODING='cp437'
DEVICE="/dev/ttyUSB0"
SAMPLE_MACHINE = {
  'host': 'nouille',
  'macAddress': '<automatique>',
  'secret': '**********',
}

SAMPLE_ACCOUNT = {
  'login': 'passoir',
  'pass': 'omgverysecure',
}

# Load data
crans_logo = Image.open(os.path.join(os.path.dirname(__file__), 'logo_crans.png'))


printer = AdafruitThermal(DEVICE, 19200, timeout=5)

def print_carac(text, value):
    printer.justify('L')
    pad = 384/12 - len(text) - len(value)
    printer.println(text + ('.'*pad) + value )


def show_entry(entry):
    printer.justify('C')
    printer.underlineOn()
    if 'host' in entry:
        title = u'Détails machine'
        if 'type' in entry:
            title += u' ' + entry['type']
        printer.println(title.encode(CODING))
    else:
        printer.println(u'Compte Crans'.encode(CODING))
    printer.underlineOff()
    printer.justify('L')

    login = entry.get('login', None) or entry['host']
    print_carac('Login', login)
    if 'macAddress' in entry:
        print_carac('Mac', entry['macAddress'])
    if 'secret' in entry:
        print_carac('Mot de passe', entry['secret'])
    if 'pass' in entry:
        print_carac('Mot de passe', entry['pass'])
    
    printer.feed(1)


# Do print
def print_liste(liste):
    printer.setDefault() # Restore printer to defaults
    printer.printImage(crans_logo, True)
    
    first = True
    
    for m in liste:
        #if not first:
        #    print_carac('','')
        first = False
        
        show_entry(m)
    
    printer.println(u'Veuillez conserver ces'.encode(CODING))
    printer.println(u'informations en lieu sûr.'.encode(CODING))
    printer.println(u' '.encode(CODING))
    printer.println(u' '.encode(CODING))
    
    printer.feed(2)

if __name__ == '__main__':
    if not sys.argv[1:]:
        liste = [SAMPLE_ACCOUNT, SAMPLE_MACHINE]
    else:
        with open(sys.argv[1], 'r') as f:
            liste = json.load(f)
    print_liste(liste)
   

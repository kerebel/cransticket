#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf-8 -*-

from __future__ import print_function
import string
import datetime
import random
import sys
from lc_ldap import crans_utils
from lc_ldap.shortcuts import lc_ldap_admin
from client import Ticket

conn = lc_ldap_admin()

def gen_password():
    """Génère un mot de passe aléatoire"""
    random.seed(datetime.datetime.now().microsecond)
    chars = string.letters + string.digits + '/=+*'
    length = 10
    return ''.join([random.choice(chars) for _ in xrange(length)])

if len(sys.argv) != 2:
    print("Veuillez taper UN login")
    exit(1)
login = sys.argv[1]
adh = conn.search(u'uid=%s' % crans_utils.escape(login), mode='rw')
if not adh:
    print("Adhérent introuvable")
    exit(1)
adh = adh[0]

try:
    value = gen_password()
    adh['userPassword'] = [crans_utils.hash_password(value).decode('ascii')]
except EnvironmentError:
    print("Impossible de changer le mot de passe de %s" % login)
    exit(2)
 
ticket = Ticket()
ticket.add_account(login, value)
ticket.print()

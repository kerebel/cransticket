# -*- coding: utf-8 -*-

from __future__ import print_function

from gestion import secrets_new as secrets
import pika
import json

CREDS = pika.credentials.PlainCredentials('oie', secrets.get('rabbitmq_oie'), True)
PARAMS = pika.ConnectionParameters(host='rabbitmq.crans.org',
    port=5671, credentials=CREDS, ssl=True)
rabbit_c = pika.BlockingConnection(PARAMS)

ch = rabbit_c.channel()
ch.queue_declare('CransTicket')

class Ticket(object):
    
    data = []
    
    def add_entry(self, x):
        self.data.append(x)

    def add_account(self, login, mdp):
        self.add_entry({'login': login, 'pass': mdp})

    def add_machine(self, machine):
        todo = {
            'host': machine['host'][0].split('.', 1)[0],
            'macAddress': machine['macAddress'][0].value,
            'type': 'fil',
        }
        if machine.has_key('ipsec'):
            todo['secret'] = machine['ipsec'][0].value
            todo['type'] = 'wifi'
        self.add_entry(todo)

    def print(self):
        ch.basic_publish(exchange='', routing_key='CransTicket', body=json.dumps(self.data))
        print("Un nouveau ticket est en cours d'impression ...")
    

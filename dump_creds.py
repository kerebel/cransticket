#!/bin/bash /usr/scripts/python.sh

from __future__ import print_function

import pika
import json
import sys

from lc_ldap.shortcuts import lc_ldap_readonly
from affich_tools import prompt
import lc_ldap.filter2 as filter

from client import Ticket

ldap = lc_ldap_readonly()

f = filter.human_to_ldap(sys.argv[1].decode('utf-8'))
res = ldap.search(f)
if len(res) > 1:
    print("More than one result")
    exit(1)
elif not res:
    print("Nobody found")
    exit(2)
else:
    item = res[0]
    item.display()
    while True:
        c = prompt("[O/N]").lower()
        if c == 'n':
            exit()
        elif c == 'o':
            break

ticket = Ticket()
if hasattr(item, 'machines'):
    for m in item.machines():
        ticket.add_machine(m)
else:
    ticket.add_machine(item)
ticket.print()

